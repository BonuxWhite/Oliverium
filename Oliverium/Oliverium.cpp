#include <fmt/format.h>
#include <gtkmm.h>

int main (int argc, char** argv)
{
    auto app = Gtk::Application::create("Meow");
    return app->make_window_and_run<Gtk::Window>(argc, argv);
}
